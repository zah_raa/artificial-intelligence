import time
import math
import heapq as hq

class Node:
    def __init__(self, state, action, parent, path_cost):
        self.state = state
        self.action = action
        self.parent = parent
        self.path_cost = path_cost
        self.f = 0
    def __lt__(self, node):
        return self.f < node.f

def goal_test(node, end, unset_balls):
    if((node.state[0] == end) and (len(node.state[1]) == 0) and (unset_balls == 0)):
        return True
    else:
        return False

def action(n, m, c, state, table, put_index, pick_index):
    actions = []
    i = state[0][0]
    j = state[0][1]
    for x in range(len(state[1])): #Put a Ball
        if i == state[1][x][2] and j == state[1][x][3]:
            put_index = x
            break
    for x in range(len(state[2])): #Pick up a Ball
        if len(state[1]) < c:
            if(i == state[2][x][0] and j == state[2][x][1]):
                actions.append('P')
                pick_index = x
                break
    if(j + 1 < m): #Right
        if(table[i][j + 1] != '*'): actions.append('R')
    if(j - 1 > -1): #Left
        if(table[i][j - 1] != '*'): actions.append('L')
    if(i + 1 < n): #Down
        if(table[i + 1][j] != '*'): actions.append('D')
    if(i - 1 > -1): #Up
        if(table[i - 1][j] != '*'): actions.append('U')
    return actions, put_index, pick_index

def take_action(node, action, pick_index):
    parent = node
    path_cost = node.path_cost + 1
    state = list(node.state)
    location = list(state[0])
    if action == 'P':
        state[1] = state[1] + (state[2][pick_index],)
        state[2] = state[2][:pick_index] + state[2][pick_index + 1:]
    if action == 'R':
        location[1] += 1
        state[0] = tuple(location)
    if action == 'D':
        location[0] += 1
        state[0] = tuple(location)
    if action == 'L':
        location[1] -= 1
        state[0] = tuple(location)
    if action == 'U':
        location[0] -= 1
        state[0] = tuple(location)
    state = tuple(state)
    return Node(state, action, parent, path_cost)

def BFS(n, m, c, start_node, end, unset_balls, table):
    all_states = 1
    actions = []
    put_index = -1
    pick_index = -1
    frontier = []
    frontier.append(start_node)
    explored = set()
    explored.add(start_node.state)
    while True:
        if not(frontier):
            return -1, all_states, len(explored)
        node = frontier.pop(0)
        put_index = -1
        pick_index = -1
        actions, put_index, pick_index = action(n, m, c, node.state, table, put_index, pick_index)
        if put_index != -1:
            state = list(node.state)
            state[1] = state[1][:put_index] + state[1][put_index + 1:]
            state = tuple(state)
            node.state = state
        for act in actions:
            all_states += 1
            new_node = take_action(node, act, pick_index)
            unset_balls = len(new_node.state[2])
            if goal_test(new_node, end, unset_balls):
                return new_node, all_states, len(explored)
            if new_node.state not in explored:
                explored.add(new_node.state)
                frontier.append(new_node)

def DLS(n, m, c, start_node, end, limit, unset_balls, table):
    all_states = 1
    actions = []
    put_index = -1
    pick_index = -1
    frontier = []
    frontier.append(start_node)
    explored = set()
    explored.add((start_node.state, 0))
    unique_explored = set()
    unique_explored.add(start_node.state)
    while True:
        if not(frontier):
            return -1, all_states, len(unique_explored)
        node = frontier.pop()
        put_index = -1
        pick_index = -1
        actions, put_index, pick_index = action(n, m, c, node.state, table, put_index, pick_index)
        if put_index != -1:
            state = list(node.state)
            state[1] = state[1][:put_index] + state[1][put_index + 1:]
            state = tuple(state)
            node.state = state
        for act in actions:
            all_states += 1
            new_node = take_action(node, act, pick_index)
            unset_balls = len(new_node.state[2])
            if goal_test(new_node, end, unset_balls):
                return new_node, all_states, len(unique_explored)
            if node.path_cost == limit:
                break
            if ((new_node.state, new_node.path_cost) not in explored) and (node.path_cost < limit):
                for i in range(node.path_cost, limit):
                    explored.add((new_node.state, i))
                unique_explored.add(new_node.state)
                frontier.append(new_node)

def IDS(n, m, c, start_node, end, unset_balls, table):
    depth = 0
    all_states = 0
    while True:
        node, states, unique_states = DLS(n, m, c, start_node, end, depth, unset_balls, table)
        all_states += states
        depth += 1
        if type(node) is Node:
            return node, all_states, unique_states

def h1(node, end):
    location = node.state[0]
    return math.sqrt(pow(location[0]-end[0], 2) + pow(location[1]-end[1], 2))

def h2(min, node):
    distance = 0
    balls_out = node.state[2]
    location = node.state[0]
    for i in range(len(balls_out)):
        distance = abs(location[0]-balls_out[i][0]) + abs(location[1]-balls_out[i][1])
        if distance < min: min = distance
    return distance + len(balls_out)

def f1(node, end, alpha):
    distance = h1(node, end)
    node.f = node.path_cost + alpha*distance
    return

def f2(min, node, alpha):
    node.f = node.path_cost + alpha*h2(min, node)
    return

def Astar(n, m, c, start_node, end, unset_balls, table, flag, alpha):
    all_states = 1
    actions = []
    put_index = -1
    pick_index = -1
    frontier = []
    frontier.append(start_node)
    explored = set()
    explored.add(start_node.state)
    while True:
        if not(frontier):
            return -1, all_states, len(explored)
        node = hq.heappop(frontier)
        put_index = -1
        pick_index = -1
        actions, put_index, pick_index = action(n, m, c, node.state, table, put_index, pick_index)
        if put_index != -1:
            state = list(node.state)
            state[1] = state[1][:put_index] + state[1][put_index + 1:]
            state = tuple(state)
            node.state = state
        for act in actions:
            all_states += 1
            new_node = take_action(node, act, pick_index)
            unset_balls = len(new_node.state[2])
            if flag == 1:
                f1(new_node, end, alpha)
            else:
                f2(n+m, new_node, alpha)
            if goal_test(new_node, end, unset_balls):
                return new_node, all_states, len(explored)
            if new_node.state not in explored:
                explored.add(new_node.state)
                hq.heappush(frontier, new_node)

def find_path(node):
    path = []
    while(node.parent != None):
        path.insert(0, node.action)
        node = node.parent
    return path

n, m = map(int, input().split())
start = ()
end = ()
start = tuple(int(x) for x in input().split())
end = tuple(int(x) for x in input().split())
c = int(input())
k = int(input())
unset_balls = k

balls = []
for i in range(0, k):
    balls.append(tuple(int(x) for x in input().split()))
balls = tuple(balls)

table = []
row = []
for i in range(n):
    row = input().split()
    table.append(row)

state = []
state.append(start)
state.append(())
state.append(balls)
state = tuple(state)

start_node = Node(state, "", None, 0)

print("BFS: ")
tic = time.time()
last_node, all_states, unique_states = BFS(n, m, c, start_node, end, unset_balls, table)
toc = time.time()
print("path cost:", last_node.path_cost)
print("all states:", all_states)
print("unique states:", unique_states)
print("time:", toc - tic)
print("path:")
print(find_path(last_node))
print("---------------------------------------------")

print("IDS: ")
tic = time.time()
last_node, all_states, unique_states = IDS(n, m, c, start_node, end, unset_balls, table)
toc = time.time()
print("path cost:", last_node.path_cost)
print("all states:", all_states)
print("unique states:", unique_states)
print("time:", toc - tic)
print("path:")
print(find_path(last_node))
print("---------------------------------------------")

print("A* using h1 and alpha = 1")
f1(start_node, end, 1)
tic = time.time()
last_node, all_states, unique_states = Astar(n, m, c, start_node, end, unset_balls, table, 1, 1)
toc = time.time()
print("path cost:", last_node.path_cost)
print("all states:", all_states)
print("unique states:", unique_states)
print("time:", toc - tic)
print("path:")
print(find_path(last_node))
print("---------------------------------------------")

print("A* using h2 and alpha = 1")
f2(m+n, start_node,1)
tic = time.time()
last_node, all_states, unique_states = Astar(n, m, c, start_node, end, unset_balls, table, 2, 1)
toc = time.time()
print("path cost:", last_node.path_cost)
print("all states:", all_states)
print("unique states:", unique_states)
print("time:", toc - tic)
print("path:")
print(find_path(last_node))
print("---------------------------------------------")

print("A* using h2 and alpha = 4.5")
f2(m+n, start_node,4.5)
tic = time.time()
last_node, all_states, unique_states = Astar(n, m, c, start_node, end, unset_balls, table, 2, 4.5)
toc = time.time()
print("path cost:", last_node.path_cost)
print("all states:", all_states)
print("unique states:", unique_states)
print("time:", toc - tic)
print("path:")
print(find_path(last_node))
print("---------------------------------------------")

print("A* using h2 and alpha = 10")
f2(m+n, start_node,10)
tic = time.time()
last_node, all_states, unique_states = Astar(n, m, c, start_node, end, unset_balls, table, 2, 10)
toc = time.time()
print("path cost:", last_node.path_cost)
print("all states:", all_states)
print("unique states:", unique_states)
print("time:", toc - tic)
print("path:")
print(find_path(last_node))
