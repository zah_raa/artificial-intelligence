import time
import random

testFile1 = "test1.txt"
testFile2 = "test2.txt"

def readInput(testFile) :
    file = open(testFile, 'r+')
    fileList = file.readlines()
    fileList = [s.replace('\n', '') for s in fileList]
    
    [days, doctors] = [int(i) for i in fileList[0].split()]
    maxCapacity = int(fileList[1])
    
    allShifts = []
    for i in range(2, days + 2):
        dayRequirements = fileList[i].split()
        morningReqs = [int(i) for i in dayRequirements[0].split(",")]
        eveningReqs = [int(i) for i in dayRequirements[1].split(",")]
        nightReqs = [int(i) for i in dayRequirements[2].split(",")]
        
        allShifts.append((morningReqs, eveningReqs, nightReqs))
    
    file.close()
    return [days, list(range(doctors)), maxCapacity, allShifts]

class Chromosome:
    def __init__(self, chrom):
        self.chromosome = chrom
        self.fitness = 0

class JobScheduler:
    def __init__(self, fileInfo):
        self.days = fileInfo[0]
        self.doctors = len(fileInfo[1])
        self.doctorsIds = fileInfo[1]
        self.maxCapacity = fileInfo[2]
        self.allShifts = fileInfo[3]
        self.popSize = 300
        self.chromosomes = self.generateInitialPopulation()
        self.elitismPercentage = 16
        self.pc = 0.65
        self.pm = 0.4
        
    def generateInitialPopulation(self):
        allChroms = []
        genetic = []
        for i in range(self.popSize):
            chrom = []
            for j in range(3*self.days):
                n = random.randint(0,self.doctors)
                genetic = random.sample(self.doctorsIds, n)
                chrom.append(genetic)
            chromosome = Chromosome(chrom)
            self.calculateFitness(chromosome)
            allChroms.append(chromosome)
        return allChroms  
    
    def crossOver(self, chrom1, chrom2):
        child1 = Chromosome([])
        child2 = Chromosome([])
        for gen1, gen2 in zip(chrom1, chrom2):
            p = random.random()
            if p < self.pc:
                child1.chromosome.append(gen1)
                child2.chromosome.append(gen2)
            else:
                child1.chromosome.append(gen2)
                child2.chromosome.append(gen1)
        return child1, child2
                
    def mutate(self, chrom):
        p = random.random()
        new_genetic = []
        if p < self.pm:
            numOfDoct = random.randint(0,self.doctors)
            new_genetic = random.sample(self.doctorsIds, numOfDoct)
            index = random.randint(0, 3*self.days-1)
            chrom.chromosome[index] = new_genetic
        return

    def calculateFitness(self, chrom):
        numOfShifts = [0] * self.doctors
        for i in range(0, len(chrom.chromosome), 3):
            for j in range(3):
                for k in range(self.doctors):
                    if k in chrom.chromosome[i+j]:
                        numOfShifts[k] += 1
                if(j != 2 and i != 0):
                    for k in range(len(chrom.chromosome[i-1])):
                        if chrom.chromosome[i-1][k] in chrom.chromosome[i+j]:
                            chrom.fitness += 1
                
                if(j == 2 and i+3 < 3*self.days and i+6 < 3*self.days):
                    for k in range(len(chrom.chromosome[j+i])):
                        if chrom.chromosome[j+i][k] in chrom.chromosome[i+3] and chrom.chromosome[j+i][k] in chrom.chromosome[i+6]:
                            chrom.fitness += 1 
                            
                if len(chrom.chromosome[i+j]) < self.allShifts[i//3][j][0]:
                    chrom.fitness += 1
                if len(chrom.chromosome[i+j]) > self.allShifts[i//3][j][1]:
                    chrom.fitness += 1
                
        for i in range(len(numOfShifts)):
            if numOfShifts[i] > self.maxCapacity:
                chrom.fitness += 1
        return
    
    def generateNewPopulation(self):
        self.chromosomes.sort(key=lambda x:x.fitness)
        bestParents = int((self.elitismPercentage/100) * self.popSize)
        new_generation = self.chromosomes[:bestParents+1]
        for i in range((self.popSize-bestParents)//2):
            parent1 = random.choice(self.chromosomes[:self.popSize//2])
            parent2 = random.choice(self.chromosomes[:self.popSize//2])
            child1, child2 = self.crossOver(parent1.chromosome, parent2.chromosome)
            self.mutate(child1)
            self.mutate(child2)
            self.calculateFitness(child1)
            self.calculateFitness(child2)
            new_generation.append(child1)
            new_generation.append(child2)
        
        self.chromosomes = new_generation[:self.popSize]
    
    def printSchedule(self):
        for i in range(0, 3*self.days, 3):
            for j in range(3):
                length = len(self.chromosomes[0].chromosome[i+j])
                if(length == 0):
                    print("empty", end=" ")
                for k in range(length):
                    if(k != length-1):
                        print(self.chromosomes[0].chromosome[i+j][k], end=",")
                    else:
                        print(self.chromosomes[0].chromosome[i+j][k], end=" ")
            print()
    
    def schedule(self): 
        while(self.chromosomes[0].fitness):
            self.generateNewPopulation()
        self.printSchedule()


fileInfo1 = readInput(testFile1)
fileInfo2 = readInput(testFile2)

tic = time.time()
scheduler = JobScheduler(fileInfo2)
scheduler.schedule()
toc = time.time()
print("time:", toc - tic)